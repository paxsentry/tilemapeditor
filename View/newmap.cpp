#include "newmap.h"
#include "ui_newmap.h"
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QtXml>

NewMap::NewMap(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewMap)
{
    ui->setupUi(this);
    SetupComboboxValues();
}

NewMap::~NewMap()
{
    delete ui;
}

void NewMap::SetupComboboxValues()
{
    m_mapSizes << "128 * 128" << "256 * 256" << "512 * 512" << "1024 * 1024" << "2048 * 2048" << "4096 * 4096";
    m_tileSizes << "16 * 16" << "32 * 32" << "64 * 64" << "128 * 128" << "256 * 256";
    ui->cmbMapSize->addItems(m_mapSizes);
    ui->cmbTileSize->addItems(m_tileSizes);

    ui->cmbMapSize->setCurrentIndex(4);
    ui->cmbTileSize->setCurrentIndex(4);
    ui->leTileLocation->setText("E:/tilemap-1k.png");
}

void NewMap::on_btnLoadTiles_clicked()
{
    m_tileLocation = QFileDialog::getOpenFileName(this, tr("Load Tiles"), "D:\\", "All files (*.*);;PNG files (*.png);;JPG files (*.jpg);;TGA files (*.tga)");
    ui->leTileLocation->setText(m_tileLocation);
}

void NewMap::on_leTileLocation_editingFinished()
{
    m_tileLocation = ui->leTileLocation->text();
}

void NewMap::on_btnSaveLocation_clicked()
{
    m_saveLocation = QFileDialog::getExistingDirectory(this, tr("Select Save Location"), "D:\\", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    ui->leSaveLocation->setText(m_saveLocation);
}

void NewMap::on_leMapName_editingFinished()
{
    m_mapName = ui->leMapName->text();
}

void NewMap::on_leSaveLocation_editingFinished()
{
    m_saveLocation = ui->leSaveLocation->text();
}

void NewMap::on_buttonBox_accepted()
{
    if (SaveEmptyMap()) {
        qInfo() << "New map file '" << m_mapName <<"' created successfully.";
        m_createdMapSize = ((ui->cmbMapSize->currentText()).left(4)).toInt();
        m_createdTileSize = ((ui->cmbTileSize->currentText()).left(3).remove(QChar('*'))).toInt();
        emit NotifyMapCreated();
    }
    else {
        qCritical() << "Can't create new map file.";
        QMessageBox::warning(this, "Error", "Can't save file!");
    }
}

void NewMap::on_NewMapCreated() { emit NotifyMapCreated(); }

bool NewMap::SaveEmptyMap()
{
    QString filenameAndLocation =  m_saveLocation + m_mapName;
    QFile newMapFile(filenameAndLocation);

    if(!newMapFile.open(QFile::WriteOnly | QFile::Text)) { return false; }

    QTextStream out(&newMapFile);
    QString data = ParseDataToXML();
    out << data;
    return true;
}

QString NewMap::ParseDataToXML()
{
    QDomDocument document;

    QDomElement root = document.createElement("TileMap");

    document.appendChild(root);

    QDomElement name = document.createElement("MapDetails");
    name.setAttribute("Name", m_mapName);
    name.setAttribute("TileSize", ui->cmbTileSize->currentText());
    name.setAttribute("MapSize", ui->cmbMapSize->currentText());
    name.setAttribute("TileLocation", m_tileLocation);
    name.setAttribute("SaveLocation", m_saveLocation);

    root.appendChild(name);

    return document.toString();
}

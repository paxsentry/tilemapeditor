#include <QMessageBox>
#include <QDebug>
#include "mainview.h"
#include "View/mapdisplay.h"
#include "View/newmap.h"
#include "View/tiledisplay.h"
#include "ui_mainview.h"

MainView::MainView(QWidget *parent, MapDisplay &mapDisplay, NewMap &newMap, TileDisplay &tileDisplay) :
    QMainWindow(parent),
    m_mapDisplay(mapDisplay),
    m_newMap(newMap),
    m_tileDisplay(tileDisplay),
    ui(new Ui::MainView)
{
    connect(&m_newMap, &NewMap::NotifyMapCreated, this, &MainView::on_newmap_created_notified);
    // connect(&m_mapDisplay, SIGNAL(Mouse_Moved()), this, SLOT(mouse_current_position()));
    // connect(&m_mapDisplay, SIGNAL(Mouse_Pressed()), this, SLOT(mouse_button_pressed()));

    ui->setupUi(this);

    m_scale = 1.0;
    m_mapDisplay.setParent(this);
    m_tileDisplay.setParent(this);
    ui->loMapDisplay->addWidget(ui->mapScroll);
    ui->tileScroll->setWidget(&m_tileDisplay);
    ui->mapScroll->setWidget(&m_mapDisplay);

    ui->statusBar->showMessage(tr("Map: Tile: | No project | Last saved: -"));

    m_mapDisplay.installEventFilter(this);

    qInfo() << "Application started:";
}

MainView::~MainView()
{
    delete ui;
}

void MainView::on_action_Exit_triggered()
{
    QApplication::quit();
}

void MainView::on_action_New_triggered()
{
    m_newMap.show();
    m_newMap.exec();
}

void MainView::on_newmap_created_notified()
{
    GetValuesFromNewMap();
    DrawBaseGrid();
}

void MainView::GetValuesFromNewMap()
{
    m_tileLocation = m_newMap.GetTileFilePath();
    m_mapName = m_newMap.GetMapName();
    m_mapSize = m_newMap.GetCreatedMapSize();
    m_tileSize = m_newMap.GetCreatedTileSize();
}

void MainView::DrawBaseGrid()
{
    UpdateMapDisplay();
    UpdateTileDisplay();
    UpdateStatusBar();
}

void MainView::UpdateMapDisplay()
{
    m_mapDisplay.setFixedSize(m_mapSize + 10, m_mapSize + 10);
    m_mapDisplay.SetMapSize(m_mapSize);
    m_mapDisplay.SetTileSize(m_tileSize);
}

void MainView::UpdateTileDisplay()
{
    m_tileDisplay.DrawTiles(m_tileLocation, m_tileSize);
}

void MainView::UpdateStatusBar()
{
    QString ms = QString::number(m_mapSize);
    QString ts = QString::number(m_tileSize);

    QString status = "Map: " + ms + "*" + ms +
                     "px Tile: " + ts + "*" + ts +
                     "px | " + m_mapName +
                     " | Last saved: -";

    ui->statusBar->showMessage(status);
    this->setWindowTitle("Tile Map Editor v 0.1 " + m_mapName);
}

void MainView::on_btnRotateLeft_clicked()
{
    m_tileDisplay.RotateImage(-90);
}

void MainView::on_btnReloadTiles_clicked()
{
    UpdateTileDisplay();
}

void MainView::on_btnRotateRight_clicked()
{
    m_tileDisplay.RotateImage(90);
}

void MainView::on_btnEraseTile_clicked()
{
    if(m_mapDisplay.IsInEditMode()) {
        QIcon pencil;
        pencil.addPixmap(QPixmap(":/root/Images/pencil.png"));
        ui->btnEraseTile->setIcon(pencil);
    }
    else {
        QIcon eraser;
        eraser.addPixmap(QPixmap(":/root/Images/erase.png"));
        ui->btnEraseTile->setIcon(eraser);
    }
}

void MainView::on_btnZoomIn_clicked()
{
    m_mapDisplay.ZoomIn(1.25);
    //ScaleWidget(1.25);
}

void MainView::on_btnZoomOut_clicked()
{
    m_mapDisplay.ZoomOut(0.8);
    //ScaleWidget(0.8);
}

void MainView::ScaleWidget(double factor)
{
    m_scale *= factor;
    ui->mapScroll->resize(m_scale * ui->mapScroll->size());

    AdjustScrollbar(ui->mapScroll->horizontalScrollBar(), factor);
    AdjustScrollbar(ui->mapScroll->verticalScrollBar(), factor);
}

void MainView::AdjustScrollbar(QScrollBar *scrollBar, double factor)
{
    scrollBar->setValue(int(factor * scrollBar->value() + ((factor -1) * scrollBar->pageStep()/2)));
}

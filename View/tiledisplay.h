#pragma once

#include <QWidget>
#include <QImage>
#include <QImageReader>
#include <QList>
#include <QStandardItemModel>
#include <QListWidgetItem>

namespace Ui {
    class TileDisplay;
}

class TileDisplay : public QWidget {
        Q_OBJECT

    public:
        explicit TileDisplay(QWidget *parent = nullptr);
        void DrawTiles(QString &tileLocation, int tileSize);
        void DisplayTiles();
        void RotateImage(int degree);
        QList<QImage> GetTiles() { return m_tiles; }
        QImage *GetCurrentTile();
        QImage GetCurrentTileCopy();

        ~TileDisplay();

    private slots:
        void on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    private:
        bool LoadTileImage(QString &fileLocation);
        void AnalyseImage(int tileSize);
        bool IsTileEmpty(QImage &tile);
        int GetCurrentSelectedIndex();

        int m_selectedForRotation;
        QImage m_tileImage;
        QList<QImage> m_tiles;

        void ClearTilesStore();
        Ui::TileDisplay *ui;
};

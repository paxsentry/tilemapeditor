#include <QDebug>
#include "mapdisplay.h"
#include "ui_mapdisplay.h"
#include "View/tiledisplay.h"

MapDisplay::MapDisplay(QWidget *parent, TileDisplay &tileDisplay) :
    QWidget(parent),
    m_tileDisplay(tileDisplay),
    ui(new Ui::MapDisplay),
    m_mapSize(0),
    m_tileSize(0),
    m_editMode(true),
    m_scale(1.0)
{
    ui->setupUi(this);
//    QPixmap empty;
//    ui->mapLabel->setPixmap(empty);
}

MapDisplay::~MapDisplay()
{
    delete ui;
}

void MapDisplay::mousePressEvent(QMouseEvent *ev)
{
    if(ev->button() == Qt::LeftButton) {
        this->m_mouseX = ev->x();
        this->m_mouseY = ev->y();

        MapCoord workItem = GetSelectedMapPosition();

        if(m_editMode) {
            UpdateCurrentMapPosition(&workItem);
        }
        else {
            RemoveTileFromMap(&workItem);
        }
        this->repaint();
    }
}

void MapDisplay::paintEvent(QPaintEvent *)
{
    if(m_mapSize > 0 && m_tileSize > 0) {
        m_map = QImage(m_mapSize, m_mapSize, QImage::Format_ARGB32);
        //QImage map(this->size(), QImage::Format_ARGB32);
        QPainter renderer(&m_map);
        QRect rect (0, 0, m_mapSize, m_mapSize);
        QPen frame(Qt::gray);
        frame.setWidth(1);

        renderer.setPen(frame);
        renderer.drawRect(rect);

        QPen linePen(Qt::lightGray);
        linePen.setWidth(1);
        linePen.setStyle(Qt::DashLine);

        for(int i = m_tileSize; i < m_mapSize; i += m_tileSize) {
            QPoint p1;
            p1.setX(i);
            p1.setY(0);
            QPoint p2;
            p2.setX(i);
            p2.setY(m_mapSize);

            renderer.setPen(linePen);
            renderer.drawLine(p1, p2);
        }

        for(int i = m_tileSize; i < m_mapSize; i += m_tileSize) {
            QPoint p3;
            p3.setX(0);
            p3.setY(i);
            QPoint p4;
            p4.setX(m_mapSize);
            p4.setY(i);

            renderer.setPen(linePen);
            renderer.drawLine(p3, p4);
        }

        if(m_mapCoords.size() > 0) {
            for(int tile = 0; tile != m_mapCoords.size(); tile++) {
                QPoint location(m_mapCoords[tile].Row * m_tileSize, m_mapCoords[tile].Col * m_tileSize);
                renderer.drawImage(location, m_mapCoords[tile].TileImage);
            }
        }
        ui->mapLabel->setPixmap(QPixmap::fromImage(m_map));
    }
}

MapCoord MapDisplay::GetSelectedMapPosition()
{
    int row = 0;
    int col = 0;
    MapCoord currentTile = { };

    for(int step = 0; step < m_mapSize; step += m_tileSize) {
        if (step + m_tileSize < m_mouseX) { row ++; }
        else {
            currentTile.Row = row;
            break;
        }
    }

    for(int step = 0; step < m_mapSize; step += m_tileSize) {
        if (step + m_tileSize < m_mouseY) { col++; }
        else {
            currentTile.Col = col;
            break;
        }
    }

    return currentTile;
}

void MapDisplay::ResizeToMapTileSize(MapCoord *mapCoord)
{
    mapCoord->TileImage.scaled(m_tileSize, m_tileSize, Qt::IgnoreAspectRatio, Qt::FastTransformation);
}

void MapDisplay::DrawTileOnMap(MapCoord *mapCoord)
{
    m_mapCoords.append(*mapCoord);
}

void MapDisplay::UpdateCurrentMapPosition(MapCoord *workItem)
{
    workItem->TileImage = m_tileDisplay.GetCurrentTileCopy();
    ResizeToMapTileSize(workItem);
    DrawTileOnMap(workItem);
}

void MapDisplay::RemoveTileFromMap(MapCoord *workItem)
{
    if(!m_mapCoords.isEmpty()) {
        int tileToClear = m_mapCoords.indexOf(*workItem, 0);

        if(tileToClear > -1) {
            m_mapCoords.remove(tileToClear);
        }
    }
}

void MapDisplay::ZoomIn(double factor)
{
    m_scale *= factor;
    auto size = m_map.size();
    auto sca = m_scale * size;

    m_map = m_map.scaled(sca);
    ui->mapLabel->repaint();
}

void MapDisplay::ZoomOut(double factor)
{
    m_scale *= factor;
    ui->mapLabel->resize(m_scale * this->size());
    ui->mapLabel->repaint();
}

#pragma once
#include <QDialog>
#include <QString>
#include <QStringList>
#include <QtGui>
#include <QtCore>

namespace Ui {
    class NewMap;
}

class NewMap : public QDialog {
        Q_OBJECT

    public:
        explicit NewMap(QWidget *parent = 0);
        ~NewMap();

        QString GetMapName() { return m_mapName; }
        QString GetTileFilePath() { return m_tileLocation ;}
        QString GetSaveLocation() { return m_saveLocation ;}

        int GetCreatedMapSize() { return m_createdMapSize; }
        int GetCreatedTileSize() { return m_createdTileSize; }

        bool SaveEmptyMap();
        QString ParseDataToXML();

    signals:
        void NotifyMapCreated();

    private slots:
        void on_btnLoadTiles_clicked();
        void on_btnSaveLocation_clicked();
        void on_leMapName_editingFinished();
        void on_buttonBox_accepted();
        void on_NewMapCreated();

        void on_leTileLocation_editingFinished();
        void on_leSaveLocation_editingFinished();

private:
        Ui::NewMap *ui;
        QStringList m_mapSizes;
        QStringList m_tileSizes;
        QString m_mapName;
        QString m_tileLocation;
        QString m_saveLocation;

        int m_createdMapSize;
        int m_createdTileSize;

        void SetupComboboxValues();
};

#include <QMessageBox>
#include <QDir>
#include <QDebug>
#include "tiledisplay.h"
#include "ui_tiledisplay.h"

TileDisplay::TileDisplay(QWidget *parent) :
    QWidget(parent),
    m_selectedForRotation(-1),
    ui(new Ui::TileDisplay)
{
    ui->setupUi(this);
}

TileDisplay::~TileDisplay()
{
    delete ui;
}

void TileDisplay::DrawTiles(QString &tileLocation, int tileSize)
{
    if(LoadTileImage(tileLocation)) {
        ClearTilesStore();
        AnalyseImage(tileSize);
        DisplayTiles();
    }
}

int TileDisplay::GetCurrentSelectedIndex()
{
    if(GetTiles().size() > 0) { return  ui->listWidget->currentIndex().row(); }
    else return -1;
}

QImage *TileDisplay::GetCurrentTile()
{
    int current = GetCurrentSelectedIndex();
    if(current >= 0) { return &m_tiles[current]; }

    return nullptr;
}

QImage TileDisplay::GetCurrentTileCopy()
{
    int current = GetCurrentSelectedIndex();
    if(current >= 0) { return m_tiles[current]; }

    return QImage();
}

bool TileDisplay::LoadTileImage(QString &fileLocation)
{
    if(!fileLocation.isEmpty()) {
        QImageReader reader(fileLocation);
        reader.setAutoTransform(true);

        QImage newImage = reader.read();

        if(newImage.isNull()) {
            qDebug() << "Tile Image file exists, but empty.";
            QMessageBox::information(this,
                                     QGuiApplication::applicationDisplayName(),
                                     tr("Cannot load file %1: %2").arg(QDir::toNativeSeparators(fileLocation), reader.errorString()));
            return false;
        }

        m_tileImage = newImage;
        return true;
    }

    qDebug() << "Empty Tile Image location parameter.";
    return false;
}

void TileDisplay::AnalyseImage(int tileSize)
{
    if(tileSize >= 16 && tileSize <= 256) {
        for(int xCoord = 0; xCoord < m_tileImage.height(); xCoord += tileSize) {
            for(int yCoord = 0; yCoord < m_tileImage.width(); yCoord += tileSize) {
                QRect tile(xCoord, yCoord, tileSize, tileSize);
                QImage tileToAdd = m_tileImage.copy(tile);

                if(!IsTileEmpty(tileToAdd)) { m_tiles.append(tileToAdd); }
            }
        }
    }
    else {
        qDebug() <<"Invalid tile size parameter.";
    }
}

bool TileDisplay::IsTileEmpty(QImage &tile)
{
    tile = tile.convertToFormat(QImage::Format_ARGB32);

    int opaquePixel = 0;

    for(int w = 0; w < tile.width(); w++) {
        for (int h = 0; h < tile.height(); h++) {
            QRgb currentPixel = (tile.pixel(w,h));
            if(qAlpha(currentPixel) !=0) {
                opaquePixel++;
                break;
            }
        }
    }

    return opaquePixel == 0;
}

void TileDisplay::DisplayTiles()
{
    ui->listWidget->clear();
    for(int i = 0; i < m_tiles.size(); i++) {
        QListWidgetItem *item = new QListWidgetItem("Tile " + QString::number(i+1), ui->listWidget);
        item->setData(Qt::DecorationRole, m_tiles[i].scaled(64, 64, Qt::IgnoreAspectRatio, Qt::FastTransformation));
        if(i == m_selectedForRotation) {
            ui->listWidget->setCurrentItem(item);
        }
    }
}

void TileDisplay::RotateImage(int degree)
{
    if(GetTiles().size() > 0) {
        QImage *tileToRotate = GetCurrentTile();
        if(tileToRotate != nullptr) {
            QTransform rotate;
            rotate.rotate(degree);
            m_selectedForRotation = ui->listWidget->currentIndex().row();
            m_tiles[m_selectedForRotation] = tileToRotate->transformed(rotate);
            DisplayTiles();
        }
    }
}

void TileDisplay::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if(current != nullptr) { current->setBackgroundColor(Qt::lightGray); }
    if(previous != nullptr) { previous->setBackgroundColor(Qt::white); }
}

void TileDisplay::ClearTilesStore()
{
    if (m_tiles.size() > 0) {
        m_tiles.clear();
    }
}

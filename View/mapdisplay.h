#pragma once
#include <QtGui>
#include <QWidget>
#include <QEvent>
#include <QMouseEvent>
#include <QImage>
#include <QPixmap>

class TileDisplay;

namespace Ui {
    class MapDisplay;
}

struct MapCoord {
    int Row;
    int Col;
    QImage TileImage;

    inline bool operator==(const MapCoord &toComapre) const
    {
        return (toComapre.Row == Row && toComapre.Col == Col);
    }
};

class MapDisplay : public QWidget {
        Q_OBJECT

    public:
        explicit MapDisplay(QWidget *parent, TileDisplay &tileDisplay);
        ~MapDisplay() override;
        void SetMapSize(int value) { m_mapSize = value; }
        void SetTileSize(int value) { m_tileSize = value; }
        int GetMousePosX() { return m_mouseX; }
        int GetMousePosY() { return m_mouseY; }
        bool IsInEditMode() { m_editMode = !m_editMode; return m_editMode; }
        void ZoomIn(double factor);
        void ZoomOut(double factor);

    protected:
        void paintEvent(QPaintEvent *) override;
        void mousePressEvent(QMouseEvent *ev) override;

        MapCoord GetSelectedMapPosition();
        void ResizeToMapTileSize(MapCoord *mapCoord);
        void DrawTileOnMap(MapCoord *mapCoord);
        void UpdateCurrentMapPosition(MapCoord *workItem);
        void RemoveTileFromMap(MapCoord *workItem);

    private:
        TileDisplay &m_tileDisplay;
        Ui::MapDisplay *ui;

        int m_mapSize;
        int m_tileSize;
        int m_mouseX;
        int m_mouseY;
        bool m_editMode;
        double m_scale;
        QImage m_map;

        QVector<MapCoord> m_mapCoords;
};

#pragma once

#include <QMainWindow>
#include <QScrollBar>

namespace Ui {
    class MainView;
}

class MapDisplay;
class NewMap;
class TileDisplay;

class MainView : public QMainWindow {
        Q_OBJECT

    public:
        explicit MainView(QWidget *parent, MapDisplay &mapDisplay, NewMap &newMap, TileDisplay &tileDisplay);
        ~MainView();

    private slots:
        void on_action_Exit_triggered();
        void on_action_New_triggered();
        void on_newmap_created_notified();
        void on_btnRotateLeft_clicked();
        void on_btnReloadTiles_clicked();

        void on_btnRotateRight_clicked();

        void on_btnEraseTile_clicked();

        void on_btnZoomIn_clicked();

        void on_btnZoomOut_clicked();

private:
        int m_mapSize;
        int m_tileSize;
        QString m_tileLocation;
        QString m_saveLocation;
        QString m_mapName;
        double m_scale;
        double m_scaleFactor;

        void DrawBaseGrid();
        void GetValuesFromNewMap();
        void UpdateMapDisplay();
        void UpdateTileDisplay();
        void UpdateStatusBar();
        void ScaleWidget(double factor);
        void AdjustScrollbar(QScrollBar *scrollBar, double factor);

        MapDisplay& m_mapDisplay;
        NewMap& m_newMap;
        TileDisplay& m_tileDisplay;
        Ui::MainView *ui;
};

#include "startup.h"
#include "View/mainview.h"
#include "View/mapdisplay.h"
#include "View/newmap.h"
#include "View/tiledisplay.h"

Startup::Startup() : QObject(nullptr),
    m_tileDisplay(*new TileDisplay(nullptr)),
    m_mapDisplay(*new MapDisplay(nullptr, m_tileDisplay)),
    m_newMap(*new NewMap(nullptr)),
    m_mainView(*new MainView(nullptr, m_mapDisplay, m_newMap, m_tileDisplay))
{

}

void Startup::Show() const
{
    m_mainView.show();
}

#pragma once

#define LOG_FILE_SIZE 1024*512
#define MAX_LOG_FILES 5

#include <QObject>
#include <QString>
#include <QDebug>

namespace Util {
    const QString _logFolder = "Logs";

    void LogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString& msg);
    void DeleteOldLogs();
    void InitLogFile();
    QString ConvertTypeToString(QtMsgType type);

    class Logger  {

        public:
            Logger();
            ~Logger();
            bool InitLogger();
    };
}

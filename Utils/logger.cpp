#include "Utils/logger.h"
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include <QDir>
#include <QFileInfoList>
#include <iostream>
#include <QDate>
#include <QTime>

namespace Util {
    static QString _logFileName;

    Logger::Logger() {}
    Logger::~Logger() {}

    bool Logger::InitLogger()
    {
        if(!QDir(_logFolder).exists()) {
            QDir().mkdir(_logFolder);
        }

        DeleteOldLogs();
        InitLogFile();

        QFile outFile(_logFileName);
        if(outFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
            qInstallMessageHandler(Util::LogMessageHandler);
            return true;
        }
        else {
            return false;
        }
    }

    void DeleteOldLogs()
    {
        QDir logDir;
        logDir.setFilter(QDir::Files | QDir:: Hidden | QDir::NoSymLinks);
        logDir.setSorting(QDir::Time | QDir::Reversed);
        logDir.setPath(_logFolder);

        QFileInfoList logList = logDir.entryInfoList();
        if(logList.size() <= MAX_LOG_FILES) {
            return;
        }
        else {
            for(int i = 0; i < (logList.size() - MAX_LOG_FILES); i++) {
                QString logPath = logList.at(i).absoluteFilePath();
                QFile logToRemove(logPath);
                logToRemove.remove();
            }
        }
    }

    void InitLogFile()
    {
        _logFileName = QString(_logFolder + "/TileMapEditor-%1.log").arg(QDate::currentDate().toString("yyyy_MMM_dd"));
    }

    void LogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
    {
        QFile outFileCheck(_logFileName);
        int logSize = outFileCheck.size();

        if(logSize > LOG_FILE_SIZE) {
            DeleteOldLogs();
            InitLogFile();
        }

        QFile outFile(_logFileName);
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream ts(&outFile);

        ts << ConvertTypeToString(type) << "@" << QTime::currentTime().toString("HH:mm:ss") << " - IN: '" << context.file << "' TEXT: " << msg << endl;
    }

    QString ConvertTypeToString(QtMsgType type)
    {
        switch (type) {
        case 0:
            return "Debug   ";
            break;
        case 1:
            return "Warning ";
            break;
        case 2:
            return "Critical";
            break;
        case 3:
            return "Fatal   ";
            break;
        case 4:
            return "Info    ";
            break;
        default:
            return "Unknown ";
            break;
        }
    }
}

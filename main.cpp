#include <QApplication>
#include <QObject>
#include "Utils/logger.h"
#include "startup.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Util::Logger logger;
    logger.InitLogger();

    QObject::connect(&a, &QApplication::lastWindowClosed, &a, &QApplication::quit);

   Startup start_up;
   start_up.Show();

    return a.exec();
}

#pragma once

#include <QObject>

class MainView;
class MapDisplay;
class NewMap;
class TileDisplay;

class Startup final : public QObject {
        Q_OBJECT

    public:
        explicit Startup();
        void Show() const;

    private:
        TileDisplay& m_tileDisplay;
        MapDisplay& m_mapDisplay;
        NewMap& m_newMap;
        MainView& m_mainView;
};
